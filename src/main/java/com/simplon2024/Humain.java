package com.simplon2024;


public class Humain {
    
    private int vie;
    private int force;
    private int xp;
    private int mana;
    private String race;


    public Humain(int vie, int force, int xp, int mana, String race) {
        this.vie = vie;
        this.force = force;
        this.xp = xp;
        this.mana = mana;
        this.race = race;
    }

    

    public int getVie() {
        return vie;
    }


    public void setVie(int vie) {
        this.vie = vie;
    }


    public int getForce() {
        return force;
    }


    public void setForce(int force) {
        this.force = force;
    }


    public int getXp() {
        return xp;
    }


    public void setXp(int xp) {
        this.xp = xp;
    }


    public int getMana() {
        return mana;
    }


    public void setMana(int mana) {
        this.mana = mana;
    }



    public String getRace() {
        return race;
    }



    public void setRace(String race) {
        this.race = race;
    }

    

    






}
