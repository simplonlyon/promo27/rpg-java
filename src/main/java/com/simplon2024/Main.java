package com.simplon2024;

public class Main {
    public static void main(String[] args) {

        Orc garrosh = new Orc(100, 100, 3, 40, "Orc");
        Humain aragorn = new Humain(100, 50, 2, 75,"Humain");
        Elfe legolas = new Elfe(100, 35, 5, 100);

        System.out.println("avant attaque "+garrosh.getVie());
        legolas.attaquer(garrosh.getVie(), garrosh.getRace());
        System.out.println("apres attaque "+garrosh.getVie());
       

    }
}