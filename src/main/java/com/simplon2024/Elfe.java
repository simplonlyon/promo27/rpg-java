package com.simplon2024;


public class Elfe {
    
    private int vie;
    private int force;
    private int xp;
    private int mana;


    public Elfe(int vie, int force, int xp, int mana) {
        this.vie = vie;
        this.force = force;
        this.xp = xp;
        this.mana = mana;
    }


    public int getVie() {
        return vie;
    }


    public void setVie(int vie) {
        this.vie = vie;
    }


    public int getForce() {
        return force;
    }


    public void setForce(int force) {
        this.force = force;
    }


    public int getXp() {
        return xp;
    }


    public void setXp(int xp) {
        this.xp = xp;
    }


    public int getMana() {
        return mana;
    }


    public void setMana(int mana) {
        this.mana = mana;
    }

    public void attaquer(int vieE, String raceE){

        if (raceE == "Orc") {
            vieE= vieE-30;
            System.out.println("Elfe attaque orc avec son arc, et l'Orc a maintenant "+ vieE + " points de vie");
  
        }
        if (raceE == "Humain") {
            vieE= vieE-40;
            System.out.println("Elfe attaque humain avec son arc, et l'humain a maintenant "+ vieE + " points de vie");
        }
        xp = xp+5;
        System.out.println("Elfe a maintenant "+ xp + " d'experience");

    }

    

    






}
